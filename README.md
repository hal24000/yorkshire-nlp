# NLP Asset Labeller

Program which enables automated assignment of asset labels.

## Usage
```bash
$ cd yorkshire-nlp/src
$ streamlit run app.py
```

## Project Organisation

    ├── chart                        <- Files required for Dimensionops build
    │
    ├── docs                         <- Documentation
    │
    ├── notebooks                    <- Jupyter notebooks
    │
    ├── src                          <- Source code for project
    │   │
    │   │── models                   <- Model code
    │   │   │── model.py             <- Model
    │   │   └── utils.py             <- Util functions
    │   │
    │   │── pages                    <- App pages
    │   │   └── page_main.py         <- Main page
    │   │
    │   │── setup                    <- Basic setup files
    │   │   │── database.py          <- Database connection
    │   │   │── favicon.ico          <- Icon
    │   │   └── layout.py            <- Layout settings
    │   │
    │   └── app.py                   <- App run file
    │   
    │── Dockerfile                   <- File to assemble a Docker image
    │
    │── environment.yml              <- Environment yml file to create conda environment
    │
    ├── Makefile                     <- Makefile for docs
    │
    ├── README.md                    <- README for this project
    │
    └── requirements.txt             <- Requirements file for creating app environment