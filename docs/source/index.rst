.. AssetLabeller documentation master file, created by
   sphinx-quickstart on Mon Aug 16 11:07:01 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to AssetLabeller's documentation!
=========================================

.. toctree::
   About <README.md>
   module_model
   module_utils
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
