import streamlit as st

from models.model import Site
from models.utils import get_table_download_link, make_train_test_csv


def run_page_main():
    """
    Main dashboard page
    """
    c1_1, _, _, _, _ = st.columns(5)
    with c1_1:
        with st.expander("Set test %"):
            test_size = st.slider("", 0, 100, 70)
            test_size /= 100
    training_csv, testing_csv = make_train_test_csv(test_size)

    with st.expander("Upload training file"):
        c2_1, c2_2, _ = st.columns(3)
        with c2_1:
            st.file_uploader("Training")
        with c2_2:
            st.subheader("")
            st.markdown(f"{len(training_csv)} entries uploaded.")
        st.dataframe(training_csv)
        st.markdown(
            f"Click {get_table_download_link(training_csv)} to download.",
            unsafe_allow_html=True,
        )
        st.markdown("Input asset info: `scada_tag`, `description`.")
        st.markdown(
            "Operator assigned labels: `tag_category`, `tag_subcategory`, `functional_area`, `process_area`."
        )

    with st.expander("Upload testing file"):
        c3_1, c3_2, _ = st.columns(3)
        with c3_1:
            st.file_uploader("Testing")
        with c3_2:
            st.subheader("")
            st.markdown(f"{len(testing_csv)} entries uploaded.")
        st.dataframe(testing_csv)
        st.markdown(
            f"Click {get_table_download_link(testing_csv)} to download file.",
            unsafe_allow_html=True,
        )
        st.markdown("Input asset info: `scada_tag`, `description`.")

    st.header("Generate NLP predictions")
    st.write("")
    if st.button("Run model"):
        with st.spinner(text="Prediction in progress..."):
            elv = Site("Elvington")
            label_df, scada_df = elv.get_df()
            df = elv.preprocess(label_df, scada_df)
            df_feat = elv.vectorize(df)
            combined_df, fig_conf = elv.predict(df, df_feat, test_size)
            final_df, fig = elv.analyse(df, combined_df)

            st.success("Done")
            st.markdown(f"{len(final_df)} predictions.")
            st.markdown("Generated predictions:")
            st.markdown(
                "`tag_category_predicted`, `tag_subcategory_predicted`, `functional_area_predicted`, `process_area_predicted`"
            )
            st.markdown("Generated probabilities:")
            st.markdown(
                "`tag_category_probability`, `tag_subcategory_probability`, `functional_area_probability`, `process_area_probability`"
            )

            with st.expander("Labelled results"):
                st.dataframe(final_df)
                st.markdown(
                    f"Click {get_table_download_link(final_df)} to download labelled results.",
                    unsafe_allow_html=True,
                )
            with st.expander("Results summary"):
                (_, c1_2, _,) = st.columns((1, 3, 1))
                with c1_2:
                    st.pyplot(fig)
            with st.expander("Confusion matrix"):
                (_, c2_2, _,) = st.columns((1, 10, 1))
                with c2_2:
                    st.pyplot(fig_conf)
