import pymongo
from pymongo import MongoClient


def db_con() -> pymongo.database.Database:
    """Connection to Mongo database."""
    db_uri = "mongodb+srv://cd_yorkshirewater:cT2k1XbwS29CYYSgNvdblvvOOumyqMju@atlas-prod-pl-0.hldmf.mongodb.net"
    client = MongoClient(db_uri)
    db_con = client["cd_yorkshirewater"]
    return db_con
