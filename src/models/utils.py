import base64
import os
import re
import string
import sys
from io import BytesIO
from typing import Tuple

import pandas
import pandas as pd
from sklearn.model_selection import train_test_split

module_path = os.path.abspath(os.path.join(""))
sys.path.append(module_path + "/src")

from setup.database import db_con


db = db_con()


def clean_text(text: str) -> str:
    """Takes text and cleans

    Args:
        text: Section of text

    Returns:
        Cleaned text
    """
    text_nonum = " ".join(
        s for s in text.split() if not any(c.isdigit() for c in s)
    )  # remove words with any digits
    text_split_upper = re.sub(
        r"((?<=[a-z])[A-Z]|(?<!\A)[A-Z](?=[a-z]))", r" \1", text_nonum
    )  # use regex to separate words with spaces then split
    text_nopunct = "".join(
        [char.lower() for char in text_split_upper if char not in string.punctuation]
    )  # remove punctuation and convert character to lower case
    text_no_doublespace = re.sub(
        "\s+", " ", text_nopunct
    ).strip()  # substitue multiple whitespace for single. Remove leading and trailing whitespace
    text_no_single = " ".join([w for w in text_no_doublespace.split() if len(w) > 2])
    return text_no_single


def get_table_download_link(df: pd.core.frame.DataFrame) -> str:
    """Generates a link allowing the data in a given Pandas dataframe to be downloaded

    Args:
        df: Dataframe of assets

    Returns:
        href string
    """
    val = to_excel(df)
    b64 = base64.b64encode(val)  # val looks like b'...'
    res = f'<a href="data:application/octet-stream;base64,{b64.decode()}" download="nlp_download.xlsx">here</a>'
    return res  # decode b'abc' => abc


def make_elvington_asset_list_clean():
    """Cleans Elvington asset data. Removes entries where there are less than 5 labels since a train/test split will likely not split correctly"""
    scada_df = pd.DataFrame(db["elvington_assets_raw"].find({}, {"_id": 0}))
    label_df = pd.DataFrame(db["site_tag_config"].find({}, {"_id": 0}))
    label_df = label_df[label_df["site_name"] == "ELVINGTON"].reset_index(drop=True)
    df = preprocess(label_df, scada_df)
    target_labels = [
        "tag_category",
        "tag_subcategory",
        "functional_area",
        "process_area",
    ]

    cutoff = 5
    for target in target_labels:
        df = df.groupby(target).filter(lambda x: len(x) > cutoff)

    for target in target_labels:
        df = df.groupby(target).filter(lambda x: len(x) > cutoff)

    df = df.reset_index(drop=True)

    for target in target_labels:
        df[f"{target}_id"] = df[target].factorize()[0]

    db["elvington_assets_clean"].drop()
    db["elvington_assets_clean"].insert_many(df.to_dict("records"))


def make_train_test_csv(
    test_size: float,
) -> Tuple[pd.core.frame.DataFrame, pd.core.frame.DataFrame]:
    """Using Elvington data, performs train/test split.

    Args:
        test_size (float): Test size split

    Returns:
        Tuple containing \m
        training_csv (Training CSV based on test split \n
        testing_csv (Testing CSV based on test split)
    """
    df = pd.DataFrame(db["elvington_assets_clean"].find({}, {"_id": 0}))
    target = df["tag_category_id"]
    target_label = "tag_category"
    # To preserve the same split for the four label sets. Set train/test split by first label
    X_train, X_test, y_train, y_test = train_test_split(
        df, target, test_size=test_size, random_state=0
    )

    training_csv = df.loc[X_train.index][
        [
            "scada_tag",
            "description",
            "tag_category",
            "tag_subcategory",
            "functional_area",
            "process_area",
        ]
    ].reset_index(drop=True)
    testing_csv = df.loc[X_test.index][["scada_tag", "description"]].reset_index(
        drop=True
    )
    return training_csv, testing_csv


def to_excel(df: pd.core.frame.DataFrame) -> bytes:
    """Takes dataframe and converts to bytes which to be downloaded

    Args:
        df: Dataframe of assets

    Returns:
        processed_data
    """
    output = BytesIO()
    writer = pd.ExcelWriter(output, engine="xlsxwriter")
    df.to_excel(writer, sheet_name="Sheet1")
    writer.save()
    processed_data = output.getvalue()
    return processed_data
