import os
import sys
from collections import OrderedDict
from typing import Tuple

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import streamlit as st
from sklearn.calibration import CalibratedClassifierCV
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.svm import LinearSVC

module_path = os.path.abspath(os.path.join(""))
sys.path.append(module_path + "/src")

from models.utils import clean_text
from setup.database import db_con


class Site:
    """NLP analysis for site"""

    company = "Yorkshire Water"
    target_labels = [
        "tag_category",
        "tag_subcategory",
        "functional_area",
        "process_area",
    ]

    def __init__(self, name):
        self.name = name

    @staticmethod
    def get_df() -> Tuple[pd.core.frame.DataFrame, pd.core.frame.DataFrame]:
        """Get true label data and scada raw data for site

        Returns:
            Tuple of label_df, scada_df
        """
        db = db_con()
        site_tags = db["site_tag_config"]
        label_df = pd.DataFrame(list(site_tags.find({}, {"_id": 0})))
        label_df = label_df[label_df["site_name"] == "ELVINGTON"].reset_index(drop=True)
        scada_df = pd.DataFrame(db["elvington_assets_raw"].find({}, {"_id": 0}))
        return label_df, scada_df

    def preprocess(
        self, label_df: pd.core.frame.DataFrame, scada_df: pd.core.frame.DataFrame
    ) -> pd.core.frame.DataFrame:
        """Preprocesses data ready for vectorization

        Args:
            label_df: Labelled dataframe
            scada_df: DataFrame from scada

        Returns:
            Scada dataframe with labels assigned
        """
        desc_dict = scada_df.set_index("TAG").to_dict()["DESCRIPTION"]
        df = label_df.copy()
        df["description"] = df["scada_tag"].apply(
            lambda x: desc_dict.get(x)
        )  # map description

        # clean features
        df.loc[:, ["sc_tag"]] = df["scada_tag"].str.replace("_", " ")
        df.loc[:, ["sc_tag"]] = df["sc_tag"].fillna(" ")
        df.loc[:, ["desc"]] = df["description"].str.replace("_", " ")
        df.loc[:, ["desc"]] = df["desc"].fillna(" ")

        # clean labels
        for label in self.target_labels:
            df.loc[:, [label]] = df[label].str.strip().replace(" ", "").str.title()

        # combine description features
        df.loc[:, ["clean_desc"]] = df["sc_tag"] + " " + df["desc"]
        df.loc[:, ["clean_desc"]] = df["clean_desc"].str.strip()
        df = df[
            [
                "scada_tag",
                "description",
                "clean_desc",
                "site_name",
                "tag_category",
                "tag_subcategory",
                "functional_area",
                "process_area",
            ]
        ]

        # Fix label inconsistencies between sites
        df = df.replace({"1-Raw Water": "1-Raw Water Intake"})
        df = df.replace({"Rapid Gravity Filters": "Rapid Gravity Filtration"})
        df["clean_desc"] = df["clean_desc"].str.replace("RGF", " RGF ")
        df["clean_desc"] = df["clean_desc"].str.replace("GAC", " GAC ")
        df["clean_desc"] = df["clean_desc"].apply(lambda x: clean_text(x))
        df["clean_desc"] = (
            df["clean_desc"]
            .str.split()
            .apply(lambda x: OrderedDict.fromkeys(x).keys())
            .str.join(" ")
        )  # remove repeated

        df = df[~df["site_name"].str.contains("0")]
        df = df.dropna(subset=self.target_labels)
        df = df.sort_values(by="site_name")
        df = df.reset_index(drop=True)

        # Remove entries below minimum threshold
        cutoff = 5
        for target in self.target_labels:
            df = df.groupby(target).filter(lambda x: len(x) > cutoff)
        for target in self.target_labels:
            df = df.groupby(target).filter(lambda x: len(x) > cutoff)
        df = df.reset_index(drop=True)

        # factorize for labels
        for target in self.target_labels:
            df[f"{target}_id"] = df[target].factorize()[0]

        return df

    def vectorize(self, df: pd.core.frame.DataFrame) -> pd.core.frame.DataFrame:
        """Runs vectorization to generate word vectors

        Args:
            df: DataFrame with 'clean_desc' columns

        Returns:
            DataFrame with vector features concatanated
        """
        feat_drop_cols = [
            "scada_tag",
            "description",
            "site_name",
            "tag_category",
            "tag_subcategory",
            "functional_area",
            "process_area",
            "tag_category_id",
            "tag_subcategory_id",
            "functional_area_id",
            "process_area_id",
        ]
        df_feat = df.drop(feat_drop_cols, axis=1)

        # no need to use idf since the text is already pretty sparse
        # sublinear tf scaling addresses the problem that 20 occurencs of a word is probably not 20 times more important than 1 occurence
        tfidf = TfidfVectorizer(use_idf=False)
        features = tfidf.fit_transform(df["clean_desc"]).toarray()
        df_feat = pd.concat([df_feat, pd.DataFrame(features)], axis=1)
        df_feat = df_feat.drop("clean_desc", axis=1)
        return df_feat

    def predict(
        self,
        df: pd.core.frame.DataFrame,
        df_feat: pd.core.frame.DataFrame,
        test_size: float,
    ) -> Tuple[pd.core.frame.DataFrame, matplotlib.figure.Figure]:
        """Splits data based on provided split, produces prediction and creates initial dataframe and confusion matrix."""
        target = df["tag_category_id"]
        # To preserve the same split for the four label sets. Set train/test split by first label
        X_train, X_test, y_train, y_test = train_test_split(
            df_feat, target, test_size=test_size, random_state=0
        )

        # labels
        df_tag_cat = df["tag_category_id"]
        df_tag_subcat = df["tag_subcategory_id"]
        df_func_area = df["functional_area_id"]
        df_proc_area = df["process_area_id"]
        target_list = [df_tag_cat, df_tag_subcat, df_func_area, df_proc_area]

        fig_conf, axs_conf = plt.subplots(4, 1, figsize=(30, 95))
        combined_df = pd.DataFrame()

        my_bar = st.progress(0)
        for i in range(len(self.target_labels)):
            target = target_list[i]
            target_label = self.target_labels[i]

            y_train = df.loc[y_train.index][f"{target_label}_id"]
            y_test = df.loc[y_test.index][f"{target_label}_id"]

            # model = RandomForestClassifier(n_estimators = 200, random_state=0)
            svm = LinearSVC(random_state=0)
            model = CalibratedClassifierCV(svm)
            model.fit(X_train, y_train)

            y_pred = model.predict(X_test)
            y_proba = model.predict_proba(X_test)

            conf_mat = confusion_matrix(y_test, y_pred)
            annotations = pd.DataFrame(conf_mat).astype(str).replace("0", "")
            annot_list = df.loc[:, [target_label]].iloc[:, 0].factorize()[1].tolist()
            if i % 2:
                # show annotations only for tag category and functional area
                sns.heatmap(
                    conf_mat,
                    fmt="s",
                    xticklabels=annot_list,
                    yticklabels=annot_list,
                    ax=axs_conf[i],
                    annot_kws={"size": 20},
                )
            else:
                sns.heatmap(
                    conf_mat,
                    annot=annotations,
                    fmt="s",
                    xticklabels=annot_list,
                    yticklabels=annot_list,
                    ax=axs_conf[i],
                    annot_kws={"size": 50},
                )
            axs_conf[i].set_title(
                f"Prediction for {target_label}. Accuracy = {int(accuracy_score(y_test, y_pred)*100)}%. n = {len(y_test)}",
                fontsize=30,
                fontweight="bold",
            )
            axs_conf[i].set_ylabel("Actual", fontsize=30)
            axs_conf[i].set_xlabel("Predicted", fontsize=30)
            axs_conf[i].set_xticklabels(annot_list, fontsize=20, rotation=90)
            axs_conf[i].set_yticklabels(annot_list, fontsize=20, rotation=0)
            plt.tight_layout()

            #### RESULTS ANALYSIS #####
            proba_df = pd.DataFrame(y_proba)
            res = pd.DataFrame(y_test)
            res.columns = ["y_test"]
            res["copy_index"] = res.index  # save index
            res = res.reset_index(drop=True)  # reset index for merging
            res = pd.concat(
                [res, pd.DataFrame(y_pred, columns=["y_pred"])], axis=1
            )  # merge predictions
            res = pd.concat([res, proba_df], axis=1)  # merge probabilities
            res = res.set_index("copy_index")  # re-instate index
            res["probability"] = res.iloc[:, 2:].max(
                axis=1
            )  # add column with maximum probability
            tag_dict = dict(
                zip(
                    pd.DataFrame(
                        df.loc[:, [target_label]].iloc[:, 0].factorize()[0]
                    ).drop_duplicates()[0],
                    annot_list,
                )
            )  # dictionary of labels
            res["actual"] = res["y_test"].map(tag_dict)
            res["predicted"] = res["y_pred"].map(tag_dict)
            res["correct"] = np.where(
                res["actual"] == res["predicted"], 1, 0
            )  # add column checking if correct

            final_res = res[["actual", "predicted", "correct", "probability"]]
            final_res.columns = [
                f"{target_label}_actual",
                f"{target_label}_predicted",
                f"{target_label}_correct",
                f"{target_label}_probability",
            ]

            combined_df = pd.concat([combined_df, final_res], axis=1)
            my_bar.progress(i / (len(self.target_labels) - 1))
            
        return combined_df, fig_conf

    def analyse(
        self, df: pd.core.frame.DataFrame, combined_df: pd.core.frame.DataFrame
    ) -> Tuple[pd.core.frame.DataFrame, matplotlib.figure.Figure]:
        """Takes results and creates final dataframe and accuracy metrics."""
        final_df = pd.DataFrame(columns=("scada_tag", "description", "clean_desc"))
        final_df = pd.concat([final_df, combined_df])
        scada_dict = dict(zip(df.index, df["scada_tag"]))
        desc_dict = dict(zip(df.index, df["description"]))
        clean_desc_dict = dict(zip(df.index, df["clean_desc"]))
        final_df["scada_tag"] = final_df.index.map(scada_dict)
        final_df["description"] = final_df.index.map(desc_dict)
        final_df["clean_desc"] = final_df.index.map(clean_desc_dict)
        final_df = final_df.drop(["clean_desc"], axis=1).reset_index(drop=True)
        final_df = final_df.drop(
            [
                "tag_category_actual",
                "tag_subcategory_actual",
                "functional_area_actual",
                "process_area_actual",
            ],
            axis=1,
        )
        probability_cols = [
            "tag_category_probability",
            "tag_subcategory_probability",
            "functional_area_probability",
            "process_area_probability",
        ]
        for col in probability_cols:
            final_df[col] = final_df[col].round(2)

        tag_cat_accuracy = int(
            100 * final_df["tag_category_correct"].value_counts()[1] / len(final_df)
        )
        tag_subcat_accuracy = int(
            100 * final_df["tag_subcategory_correct"].value_counts()[1] / len(final_df)
        )
        func_area_accuracy = int(
            100 * final_df["functional_area_correct"].value_counts()[1] / len(final_df)
        )
        proc_area_accuracy = int(
            100 * final_df["process_area_correct"].value_counts()[1] / len(final_df)
        )

        correct_cols = [
            "tag_category_correct",
            "tag_subcategory_correct",
            "functional_area_correct",
            "process_area_correct",
        ]
        for col in correct_cols:
            final_df[col] = np.where(final_df[col] == 1, "Yes", "No")

        accuracy_df = pd.DataFrame(
            {
                "Label": [
                    "Tag category",
                    "Tag subcategory",
                    "Functional area",
                    "Process area",
                ],
                "Accuracy": [
                    tag_cat_accuracy,
                    tag_subcat_accuracy,
                    func_area_accuracy,
                    proc_area_accuracy,
                ],
            }
        )

        fig, ax = plt.subplots(figsize=(6, 4))
        ax = sns.barplot(x="Label", y="Accuracy", data=accuracy_df)
        fig.suptitle(
            f"Prediction Accuracy for Asset Label Model ({len(final_df)} predictions)"
        )
        plt.tight_layout()

        for p in ax.patches:
            ax.annotate(
                format(p.get_height(), ".0f"),
                (p.get_x() + p.get_width() / 2.0, p.get_height()),
                ha="center",
                va="center",
                xytext=(0, -60),
                textcoords="offset points",
            )
        return final_df, fig
