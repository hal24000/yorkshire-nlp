import streamlit as st

from pages.page_main import run_page_main
from setup.layout import setup_page


setup_page("NLP Asset Labeller")

run_page_main()

with st.sidebar:
    st.header("")
    st.info(
        """
    ### :information_source: NLP Asset Labelling
    - Natural Language Processing to accelerate digital twinning for water processing plants.
    """
    )
